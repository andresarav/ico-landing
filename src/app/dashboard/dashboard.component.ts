import { Component, OnInit } from '@angular/core';
import Pusher from 'pusher-js';
import { CmcService } from '../cmc.service';
// import ClipboardJS  from 'clipboard';
import { User, Transaction } from '../models/modelos.model';
import Countdown from 'countdown';
import { POH } from '../models/modelos.model';

import * as $ from 'jquery';
import Chart from 'chart.js';

import { ngCopy } from 'angular-6-clipboard';
import {MatSnackBar} from '@angular/material';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {


constructor(
  public cmcService:CmcService,
  public snackBar:MatSnackBar
){
  this.pusher = new Pusher('de504dc5763aeef9ff52');
  var channelCoin1 = this.pusher.subscribe(`live_trades_ethusd`);
  channelCoin1.bind('trade', (data)=>{
    if(this.coinAbrev.toLowerCase() == 'eth'){
      return this.coinPrice = data.price.toFixed(0)
    }
 });

 var pusher2 = new Pusher('de504dc5763aeef9ff52');
 var channelCoin2 = pusher2.subscribe(`live_trades_ltcusd`);
 channelCoin2.bind('trade', (data)=>{
   if(this.coinAbrev.toLowerCase() == 'ltc'){
     return this.coinPrice = data.price.toFixed(0)
   }
});

var pusher3 = new Pusher('de504dc5763aeef9ff52');
var channelCoin3 = pusher3.subscribe(`live_trades`);
channelCoin3.bind('trade', (data)=>{
    if(this.coinAbrev.toLowerCase() == 'btc'){
      return this.coinPrice = data.price.toFixed(0)
    }
});

// ClipboardJS

  // this.transaction = this.transactionUser;

  if(this.transaction){
    this.calcdef = false;
    this.step = 2;
    this.goto(2, this.transaction.currency.toLowerCase());

    setTimeout(()=>{
      this.cargarChart()
    }, 1500)
  }

  var timerId = new Countdown( this.pohinfo.endDay, (ts)=> {this.fecha = ts},   Countdown.DEFAULTS);

  // setInterval(()=>{
  //   var fecha = new Countdown( this.pohinfo.startDay, this.pohinfo.endDay, Countdown.DEFAULTS);
  //   console.log('Que lo que cabrón!!');
  //   console.log(fecha);
  // }, 1000)

  this.calcularFechas(this.fecha)

  // setInterval(()=>{
  // }, 1000)
}


fecha:any;
fecha2:any;
invertir:number = 1;
transaction:Transaction;
avanced:any;
pohinfo:POH = new POH(
  new Date(2018, 7, 1),
  new Date(2018, 10, 3)
)

user:User = new User(
  223332989,
  new Date(),
  'Andres1983',
  'construyacol@gmail.com',
  'https://d3iw72m71ie81c.cloudfront.net/male-49.jpg',
  'Andres Felipe',
  'Guevara García',
  'Colombia',
  '0x3635e381c67252405c1c0e550973155832d5e490',
   null
)


transactionUser:Transaction = new Transaction(
  this.user.uid,
  'success',
  'eth', //ERC20
  'ethereum',
  7,
  400,
  13123123123123,
  123123123123123,
  1222,
  '0x3a4585d50687f7926d88d72280bf0c68663fd944fd248a43d15fab3ee15eec90',
  'sin info adicional',
  'sin comentarios',
  '0x002ec2228024fc71dadabc57b9a62c466a2880c7',
);

// pusher configuration
channelEth;
pusher;
channelLtc;
channelBtc;
channelCoin;

active:boolean = true;
step:number = 1;
coin:string = 'BTC'
coinAbrev:string ="ETH";
calcdef:boolean = true;
price:any;
ethPrice:number=0;
litePrice:number=0;
btcPrice:number=0;
coinPrice:number;
loader:boolean = false;
loader2:boolean = true;
reclaim:boolean;


a:number=7;
datas=[ 0, 7, 14, 21, 28, 35, 42, 57, 72, 87, 102, 117, 132];
datas2 = [ 0, 0, 0, 0, 7, 14, 21];
labels = ["Distribution 1 Month(1)", "Distribution 1 Month(2)", "Distribution 1 Month(3)", "Distribution 1 Month(4)", "Distribution 1 Month(5)", "Distribution 1 Month(6)", "Distribution2/7", "Distribution2/8", "Distribution2/9", "Distribution2/10", "Distribution2/11", "Distribution2/12"];
days;
daysTrue:boolean = false;


myChart;

  ngOnInit() {}



copiar(){
  ngCopy(this.user.account_to_deposit);
  this.openSnackBar('La dirección se ha copiado en el portapapeles', '¡EXITO!')
}


openSnackBar(message: string, action: string) {
  this.snackBar.open(message, action, {
    duration: 5000,
    panelClass: ['blue-snackbar']
  });
}


calcularFechas(fechaRes){

  var inicio = this.pohinfo.startDay.getTime();
  var fin = this.pohinfo.endDay.getTime();
  var diasTotales = (fin - inicio)/(1000*60*60*24);

  var fechaHoy = new Date().getTime();
  var diasRestantes = (fin - fechaHoy)/(1000*60*60*24);
  this.days = diasRestantes.toFixed(0);

  if(this.days>0){
    this.daysTrue = true;
  }

  var tasaPer = ((this.days*100)/diasTotales);
  this.avanced = 100 - tasaPer;
  // this.avanced = 100;

  if(this.avanced > 99.9){
  // if(this.avanced > 99.9 && fechaRes.hours < 1){
    this.reclaim = true;
  }

//   setInterval(()=>{
//     console.log('Días Totales');
//     console.log(diasTotales);
//     console.log('Días restantes');
//     console.log(diasRestantes);
//     console.log('Tasa porcentual');
//     console.log(this.avanced);
//     console.log(fechaRes);
// }, 1000)


  // console.log(diff/(1000*60*60*24) );

}




cargarChart(){
  var ctx = document.getElementById("myChart2");

  var myChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: this.labels,
    datasets: [{
          data:this.datas,
          backgroundColor: ['rgb(0, 204, 172, 0.2)'],
          borderColor: ['rgb(0, 204, 172)'],
          borderWidth: 3,
      },
      // {
      //       data:this.datas2,
      //       backgroundColor: ['rgb(115, 146, 177, 0.2)'],
      //       borderColor: ['rgb(0, 204, 172)'],
      //       borderWidth: 3,
      //       spanGaps:false
      //   },
      ]
  },
  options: {
    elements: {
            line: {
                tension: 0, // disables bezier curves
            }
        },
    legend:{
      display: false
    },
    scales: {
          xAxes: [{
              ticks: {
                callback: function(value, index, values) {
                    return '';
                }
              }
            }],
            yAxes: [{
                ticks: {
                  callback: function(value, index, values) {
                      return '';
                  }
                }
              }],
      }
  }
});
}


goto(step, coin){

  this.step = step;
  this.coin = coin;
  this.loader = true;


setTimeout(async()=>{


  switch(coin.toLowerCase()) {
      case 'ethereum':
          this.coinAbrev = 'ETH';
          this.coinPrice = this.ethPrice;
          this.loader = false;
          this.consularCurrency('eth');
          break;
      case 'bitcoin':
          this.coinAbrev = 'BTC';
          this.coinPrice = this.btcPrice;
          this.loader = false;
          this.consularCurrency('btc');
          break;
      case 'litecoin':
          this.coinAbrev = 'LTC';
          this.coinPrice = this.litePrice;
          this.loader = false;
          this.consularCurrency('ltc');
          break;
      default:
      console.log(false);
  }
  console.log('esta es la abreviatura: ', this.coinAbrev);

},800)


}


async consularCurrency(coin){


  var coinR = await JSON.parse(localStorage.getItem(coin));

  console.log('esto es lo que trae de localstorage', coinR);
       this.coinPrice = coinR;
       this.loader2 = false;
}


}
