import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import {MatSnackBar} from '@angular/material';
import 'rxjs';
import * as $ from 'jquery';
import qs from 'querystringify';


@Injectable({
  providedIn: 'root'
})


export class CmcService {
urlApiCMC:any;
urlApiBTP:any;

  constructor(
    public http: Http,
    public snackBar: MatSnackBar
  ) {
    this.urlApiCMC = 'https://api.coinmarketcap.com/v2/ticker/';
    this.urlApiBTP = 'https://www.bitstamp.net/api/v2/ticker/';

  }


          getProjectDetailBTP(coin): Promise<any>{
            return new Promise((resolve, rej)=>{
              const headers = new Headers({ 'content-type': 'text/plain' });
              var url = `${this.urlApiBTP}${coin}usd`;
              this.http.get(url, {headers: headers}).toPromise()
              .then((res:any) => {
                try{
                  res = JSON.parse(res._body);
                  return resolve(res.last);
                } catch (e){
                }
              })
              .catch((err) => {
                return rej(err)
                // setTimeout(()=>{
                  // return this.getProjectDetailBTP(coin);
                // },5000)
              });
              })
          }


          number_format(amount) {
              amount += ''; // por si pasan un numero en vez de un string
              amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto
              amount = '' + amount.toFixed(0);
              var amount_parts = amount.split('.'),
                  regexp = /(\d+)(\d{3})/;
              while (regexp.test(amount_parts[0]))
                  amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');
              return amount_parts.join('.');
          }


          ubicar(id){

                     var elemento5 = $(id);
                     var posicionbar = elemento5.position();
                     var altura=posicionbar.top;
                     console.log(elemento5);

                     $('body, html').animate({
                       scrollTop: altura - 60
                     }, 700);
          }


    openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 20000,
      panelClass: ['blue-snackbar']
    });
  }

}
