import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatToolbarModule,
  MatMenuModule,
  MatSlideToggleModule,
  MatIconModule,
  MatTableModule,
  MatFormFieldModule,
  MatExpansionModule,
  MatListModule,
  MatInputModule,
  MatTabsModule,
  MatDividerModule,
  MatTooltipModule,
  MatDialogModule,
  MatSnackBarModule,
  MatAutocompleteModule,
  MatRadioModule,
  MatSortModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatProgressSpinnerModule,
  MatSelectModule

  } from "@angular/material";

  // import { DialogContentExampleDialog, ProjectListComponent } from './admin/lists/projectList/projectList.component';


// import {MatDialogModule} from '@angular/material/dialog';

const modules = [MatSelectModule,  MatProgressSpinnerModule, MatNativeDateModule, MatDatepickerModule, MatSortModule, MatRadioModule, MatAutocompleteModule, MatSnackBarModule, MatDialogModule, MatDividerModule, MatTabsModule, MatInputModule, MatTooltipModule, MatListModule, MatExpansionModule, MatFormFieldModule, MatTableModule, MatIconModule, MatMenuModule, MatToolbarModule, MatSlideToggleModule, MatMenuModule, MatButtonModule, MatCheckboxModule ];

@NgModule({
  imports: modules,
  exports: modules,
})
export class materialModule { }
