import { Component, OnInit, ViewChild } from '@angular/core';
import * as $ from 'jquery';
import {Power1, Bounce} from 'gsap/all';
declare var TweenMax: any;
declare let ScrollMagic: any;
import Countdown from 'countdown';
import Tilt from 'tilt.js';
import Chart from 'chart.js';

import { Router } from '@angular/router';
import Pusher from 'pusher-js';


// declare var tilt:any;
import {CmcService} from '../cmc.service';



@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})

export class LandingComponent implements OnInit {

lengua:any="Español";

ethPrice:number = 0;
landing:boolean=true;
public controller :any;


  constructor(
    public cmcService:CmcService,
    private router: Router
  ){

    // this.pusher = new Pusher('de504dc5763aeef9ff52');
    this.consularCurrencys()

    var timerId = new Countdown( new Date(2018, 12, 3), (ts)=> {this.fecha = ts}, Countdown.DEFAULTS)

    $.getScript( "./assets/js/three.min.js", ()=>{
      $.getScript( "./assets/js/Projector.js", ()=>{
        $.getScript( "./assets/js/CanvasRenderer.js", ()=>{
          $.getScript( "./assets/js/index.js", ()=>{});
        });
      });
    });
  }


  coinPrice;
  invertir;
  channelCoin;
  pusher;


async consularCurrencys(){
  await this.consultarCurrency('eth');
  await this.consultarCurrency('btc');
  await this.consultarCurrency('ltc');
}


  async consultarCurrency(coin){
      try{
        let res = await this.cmcService.getProjectDetailBTP(coin);
        localStorage.setItem(coin, JSON.stringify(res));

        if(coin=='eth'){
          this.ethPrice = res;
        }
        return res;
      } catch (e){
        setTimeout(() => {
          this.consultarCurrency(coin);
        }, 2000);
      }
    }


  ira(){
    this.landing = false;
    setTimeout(()=>{
      this.router.navigate(['/session'])
    },400)
  }

  fecha;
  // labels=['0% Fee', '-25% Fee', '0% Fee', 'Dristribution1', 'Dristribution2' ];
  labels=['0% Fee', '-25% Fee'];



  datas=[-250, 0];
  datas2=[];
  datas3=[];

  first:number=0;
  second:number=0;
  third:number=0;
  four:number=0;
  five:number=0;

    bajarVP(id){
           var elemento5 = $(id);
           var posicionbar = elemento5.position();
           var altura=posicionbar.top;

           $('body, html').animate({
             scrollTop: altura
           }, 400);
    }








  ngOnInit(){

// Tilt



// PEGAMOS EL MENU Y LE DAMOS OPACIDAD AL HACER SCROLL----------------------------------------------------------

    this.controller = new ScrollMagic.Controller();

    var scenita = new ScrollMagic.Scene({
      triggerHook:0,
      offset:30
   })
            .setClassToggle('#menu', 'pegateEsta')
            // .addIndicators()
            .addTo(this.controller);

            scenita.on('start', (event) => {
              // $('.bagro').toggleClass("aparece2");
            })


// MENU ANIM AFTER
this.controller = new ScrollMagic.Controller();

var scenita2 = new ScrollMagic.Scene({
  triggerHook:0,
  offset:500,
})
        // .addIndicators()
        .addTo(this.controller);

        scenita2.on('start', (event) => {
          $('.optioni').toggleClass("fitOp");
          $('.ctass').toggleClass("apareceCta");
          $('.ctass2').toggleClass("comoFua");
          // $('.ctass').toggleClass("animar");
          // $('.bagro').toggleClass("aparece2");
          $('.logoCont').toggleClass("logoContBefore");
          $('.logoContCont').toggleClass("logoContContBefore");
          $('.logoContCont').toggleClass("logoContContBefore2");
        })

        // MENU ANIM AFTER 2 progresive
        this.controller = new ScrollMagic.Controller();

        var scenita3 = new ScrollMagic.Scene({
          triggerHook:0,
          offset:0,
          duration:500
        })
                // .addIndicators()
                .setTween(TweenMax.to(".bagro", 1, {opacity:1, ease: Power1.easeOut} ))
                .addTo(this.controller);




  if(screen.width>800){



  var ctx = document.getElementById("myChart2");
  var myChart = new Chart(ctx, {
  type: 'line',
  data: {
    // labels: ['1','1','1','1','1','1','1','1','1','1','1','1','1','1'],
    labels: this.labels,
    datasets: [{
          data:this.datas,
          backgroundColor: ['rgb(115, 146, 177, 0.2)'],
          borderColor: ['rgb(0, 204, 172)'],
          borderWidth: 3,
      },{
            data:this.datas2,
            borderColor: ['rgb(0, 204, 172)'],
            borderWidth: 3,
            spanGaps:false
        },
        {
              data:this.datas3,
              borderColor: ['rgb(0, 204, 172)'],
              borderWidth: 3,
              spanGaps:false
          }]
  },
  options: {
    elements: {
            line: {
                tension: 0, // disables bezier curves
            }
        },
    legend:{
      display: false
    },
    scales: {
          xAxes: [{
              ticks: {
                callback: function(value, index, values) {
                    return '';
                }
              }
            }],
            yAxes: [{
                ticks: {
                  callback: function(value, index, values) {
                      return '';
                  }
                }
              }],
      }
  }
});






// DEJAMOS PEGADIZO LAS SECCION DE LAS ETAPAS DE INVERSIÓN, EN PARALELO A EL CHART---------------------------------------

    this.controller = new ScrollMagic.Controller();

    var scene = new ScrollMagic.Scene({
      triggerElement: "#section3",
      triggerHook:0,
   })
            .setClassToggle('#backgroundCont', 'pegatEsta')
            .addTo(this.controller);


// DEJAMOS PEGADIZA EL CHART ---------------------------------------------------------------------------
            var scene20 = new ScrollMagic.Scene({
              triggerElement: "#section3",
              triggerHook:0,
           })
                    .setClassToggle('.contenedorChart', 'pegado')
                    .addTo(this.controller);


// animación Chart y etapas del POH---------------------------------------------------------------------------------------------------------------------------------------


     //DIPARADOR UNO ------------------------------------------------------------------------------------------------------------------------------------

                    var scene21 = new ScrollMagic.Scene({
                      triggerElement: "#etapa2",
                      triggerHook:0.2,
                   })
                            .addTo(this.controller);

            scene21.on('start', (event) => {
              $('.etapaT2').toggleClass("aparece");
              $('.tapita1').toggleClass("desaparece");
              $('.e3').toggleClass("aparece");
              $('.e4').toggleClass("aparece");
              $('.element2').addClass("element2Pos1");
              $('.element3').addClass("aparece");
              $('.element5').addClass("aparece");

              if(this.first<1){
                this.labels.push('Distribution 1');
                this.datas.push(250);
                myChart.update();
                return this.first++;
              }
              return false
            })


    //DIPARADOR DOS ------------------------------------------------------------------------------------------------------------------------------------

              var scene22 = new ScrollMagic.Scene({
                triggerElement: "#etapa3",
                triggerHook:0.2,
             })
                      .addTo(this.controller);

      scene22.on('start', (event) => {

        $('.e5').toggleClass("aparece");
        $('.e4').toggleClass("desaparece");
        $('.element2').addClass("element2Pos2");
        $('.element3').addClass("element3Pos1");
        $('.element4').addClass("aparece");
        $('.element5').addClass("element5Pos1");
        $('.element8').addClass("aparece");
        $('.element9').addClass("aparece");



        if(this.second<1){
          this.labels.push('Distribution 1');
          this.datas.push(500);
          myChart.update();
          return this.second++;
        }
        return false
      })



      //DIPARADOR TRES ------------------------------------------------------------------------------------------------------------------------------------

                var scene23 = new ScrollMagic.Scene({
                  triggerElement: "#etapa4",
                  triggerHook:0.2,
               })
                        // .addIndicators()
                        .addTo(this.controller);

        scene23.on('start', (event) => {

          $('.e2').toggleClass("aparece");
          $('.e5').toggleClass("desaparece");
          $('.e3').toggleClass("desaparece");
          $('.element6').addClass("aparece");



          if(this.third<1){
            this.datas2.push('ss', 0, 125);
            myChart.update();
            return this.third++;
          }
          return false
        })


        //DIPARADOR CUATRO ------------------------------------------------------------------------------------------------------------------------------------

                  var scene24 = new ScrollMagic.Scene({
                    triggerElement: "#etapa5",
                    triggerHook:0.2,
                 })
                          // .addIndicators()
                          .addTo(this.controller);


          scene24.on('start', (event) => {
            $('.e2').toggleClass("desaparece");
            $('.e1').toggleClass("aparece");
            $('.element7').addClass("aparece");


            if(this.four<1){
              this.datas3.push('ss', 0, 62);
              this.datas.push(60);
              myChart.update();
              return this.four++;
            }
            return false
          })




      // seccion POH ACTIVAR----------------------------------------------------------

          this.controller = new ScrollMagic.Controller();

          var poh = new ScrollMagic.Scene({
            triggerElement: "#entry",
            triggerHook:0.2,
         })
                  // .setClassToggle('#menu', 'pegateEsta')
                  // .addIndicators()
                  .addTo(this.controller);

                  poh.on('start', (event) => {
                    $('.poh').toggleClass("itemBefo");
                  })


        // seccion POH DESACTIVAR----------------------------------------------------------

            this.controller = new ScrollMagic.Controller();

            var poh2 = new ScrollMagic.Scene({
              triggerElement: "#section6",
              triggerHook:0.2,
           })
                    // .setClassToggle('#menu', 'pegateEsta')
                    // .addIndicators()
                    .addTo(this.controller);

                    poh2.on('start', (event) => {
                      $('.poh').toggleClass("itemBefo");
                    })







    // seccion Road ACTIVAR----------------------------------------------------------

        this.controller = new ScrollMagic.Controller();

        var road = new ScrollMagic.Scene({
          triggerElement: "#roadMap",
          triggerHook:0.2,
       })
                // .addIndicators()
                .addTo(this.controller);

                road.on('start', (event) => {
                  $('.road').toggleClass("itemBefo");
                })


          // seccion Road DESACTIVAR----------------------------------------------------------

              this.controller = new ScrollMagic.Controller();

              var road2 = new ScrollMagic.Scene({
                triggerElement: "#members",
                triggerHook:0.2,
             })
                      // .addIndicators()
                      .addTo(this.controller);

                      road2.on('start', (event) => {
                        $('.road').toggleClass("itemBefo");
                      })




      // seccion member ACTIVAR----------------------------------------------------------

          this.controller = new ScrollMagic.Controller();

          var road = new ScrollMagic.Scene({
            triggerElement: "#members",
            triggerHook:0.2,
         })
                  // .addIndicators()
                  .addTo(this.controller);

                  road.on('start', (event) => {
                    $('.team').toggleClass("itemBefo");
                  })


          // seccion member DESACTIVAR----------------------------------------------------------

              this.controller = new ScrollMagic.Controller();

              var road2 = new ScrollMagic.Scene({
                triggerElement: "#footer",
                triggerHook:0.2,
             })
                      // .addIndicators()
                      .addTo(this.controller);

                      road2.on('start', (event) => {
                        $('.team').toggleClass("itemBefo");
                      })



// // DEJAMOS PEGADIZA LA PANTALLA QUE CONTIENE LAS IMAGENES
 //  var scene2 = new ScrollMagic.Scene({
 //    triggerElement: "#section3",
 //    triggerHook:0,
 // })
 //          // .setTween(TweenMax.to("#btcPriceCont", 2, {opacity: 0, ease: Power1.easeOut}))
 //          // .setPin('#pegalo', {pushFollowers: false})
 //          .setClassToggle('#pegalo', 'pegado')
 //          // .addIndicators()
 //          .addTo(this.controller);


      //     var scene21 = new ScrollMagic.Scene({
      //       triggerElement: "#etapa5",
      //       triggerHook:0.4,
      //       // duration:this.lanza()
      //    })
      //             // .setTween(TweenMax.to("#btcPriceCont", 2, {opacity: 0, ease: Power1.easeOut}))
      //             // .setPin('#pegalo', {pushFollowers: false})
      //             .addIndicators({name: 'Disparador4'})
      //             .addTo(this.controller);
      //
      //
      // scene21.on('start', (event) => {
      //   $('.e3').toggleClass("aparece");
      //   $('.e2').toggleClass("desaparece");
      //
      //
      //   if(this.second<1){
      //     this.labels.push('Distribution 1');
      //     // this.labels.push('Distribution 2');
      //     this.datas.push(60);
      //     myChart.update();
      //     return this.second++;
      //   }
      //
      //   // console.log(this.first);
      //   // this.first--;
      //   // return this.datas.splice(1,1);
      //   return false
      // })







//AQUI TERMINA LAS ETAPAS DEL POH------------------------------------------------------------------------------------------------------------




        // // DEJAMOS PEGADIZA EL CHART QUE CONTIENE LAS IMAGENES
        //     var scene22 = new ScrollMagic.Scene({
        //       triggerElement: "#etapa2",
        //       triggerHook:0.2
        //    })
        //             // .setTween(TweenMax.to("#btcPriceCont", 2, {opacity: 0, ease: Power1.easeOut}))
        //             // .setPin('#pegalo', {pushFollowers: false})
        //             .addIndicators({name: 'scene22'})
        //             .addTo(this.controller);
        //
        //     scene22.on('start', (event) => {
        //       if(this.second<1){
        //         // this.datas.push(Math.random());
        //         this.datas.push(500);
        //         myChart.update();
        //         console.log(this.first);
        //         return this.second++;
        //       }
        //
        //       return false
        //     })













    // // DEJAMOS PEGADIZA LA PANTALLA QUE CONTIENE LAS IMAGENES
    //     var scene3 = new ScrollMagic.Scene({
    //       triggerElement: "#section3",
    //       triggerHook:0,
    //    })
    //             // .setTween(TweenMax.to("#btcPriceCont", 2, {opacity: 0, ease: Power1.easeOut}))
    //             // .setPin('#pegalo', {pushFollowers: false})
    //             .setClassToggle('#encabezado', 'ciao')
    //             // .addIndicators()
    //             .addTo(this.controller);



// DESAPARECEMOS EL FONDO DE LAS ONDAS CUANDO TOCAMOS LA #SECTION4
   //  var scene4 = new ScrollMagic.Scene({
   //    triggerElement: "#section4",
   //    triggerHook:0.6,
   // })
   //          // .setTween(TweenMax.to("#btcPriceCont", 2, {opacity: 0, ease: Power1.easeOut}))
   //          .setClassToggle('#backgroundCont', 'desaparecer')
   //          // .addIndicators()
   //          .addTo(this.controller);



    // //DISPARADOR, PRIMER SLIDE DENTRO DE LA PANTALLA 3D
    //     var scene5 = new ScrollMagic.Scene({
    //       triggerElement: "#contenido2",
    //       triggerHook:0.3,
    //    })
    //             // .setTween(TweenMax.to("#btcPriceCont", 2, {opacity: 0, ease: Power1.easeOut}))
    //             .setClassToggle('.contVentana', 'suba')
    //             // .addIndicators()
    //             .addTo(this.controller);
    //
    // //DISPARADOR, segundo SLIDE DENTRO DE LA PANTALLA 3D
    //     var scene5 = new ScrollMagic.Scene({
    //       triggerElement: "#contenido3",
    //       triggerHook:0.3,
    //    })
    //             // .setTween(TweenMax.to("#btcPriceCont", 2, {opacity: 0, ease: Power1.easeOut}))
    //             .setClassToggle('.contVentana', 'suba2')
    //             // .addIndicators()
    //             .addTo(this.controller);




    // Tilt;
    // const tilts = $('.contentImgs').tilt({
    //   maxTilt: 40,
    //   glare: true,
    //   maxGlare: 0.3,
    //   scale: 1.1,
    //   transition:true,
    //   speed: 2000,
    //   easing: "cubic-bezier(.03,.98,.52,.99)",
    //   perspective:1000,
    //   reset:true
    // });




  // this.foco('foco');


    // console.log('jquery', $)
    // console.log('scrollMagic', ScrollMagic)
    // console.log('gsap', Power1, Bounce)
    // console.log('greensock', TweenMax)

    // var cuenta = new Countdown( new Date(2018, 0, 1), new Date(2018, 4, 3));
        // var cuenta = new Countdown(507314280000, null, Countdown.DAYS)


        // this.segundos = cuenta.seconds;
        // setInterval(()=>{
        //   console.log('REPRESETA --------------------------------',   this.fecha.seconds)
        //   console.log(this.ethPrice);
        //
        // },1000)
          }
  }


  title = 'app';

foco(idElemento){
 // return document.getElementById(idElemento).select();
}

  addFormat(amount){
    this.cmcService.number_format(amount);
  }

}
