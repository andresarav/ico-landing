export class User{
constructor(
  public uid?:any,
  public createdAt?:any,
  public username?:any,
  public email?:any,
  public cover?:any,
  public name?:any,
  public lastName?:any,
  public country?:any,
  public account_to_deposit?:any,
  public transaction?:Transaction
){}
}

export class Transaction{
  constructor(
  public userId?:string,
  public state?:string,
  public currency_type?:string,
  public currency?:string,
  public amount?:any,
  public amount_neto?:any,
  public cost_id?:any,
  public deposit_provider_id?:any,
  public deposit_cost?:any,
  public proof_of_payment?:any,
  public info?:any,
  public comment?:string,
  public account_id?:string,
  public expiration_date?:Date,
  public creation_date?:Date,
  public update_date?:Date
  ){}
}

export class POH{
  constructor(
    public startDay?:Date,
    public endDay?:Date
  ){}
}
