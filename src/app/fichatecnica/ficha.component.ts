import { Component, OnInit } from '@angular/core';
import Chart from 'chart.js';
import Tilt from 'tilt.js';
import * as $ from 'jquery';


@Component({
  selector: 'app-ficha',
  templateUrl: './ficha.component.html',
  styleUrls: ['./ficha.component.css']
})

export class FichaComponent implements OnInit {

  constructor() { }

  ngOnInit() {



Tilt;
const tilts = $('.percent').tilt({
  maxTilt: 40,
  glare: false,
  maxGlare: 0.3,
  scale: 1.2,
  transition:true,
  speed: 2000,
  easing: "cubic-bezier(.03,.98,.52,.99)",
  perspective:1000,
  reset:true
});

  }

}
