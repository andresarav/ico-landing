import { RouterModule, Routes } from '@angular/router';

import { AuthComponent } from './auth/auth.component';
import { LandingComponent } from './landing/landing.component';
import { DashboardComponent } from './dashboard/dashboard.component';

// import { ADMIN_ROUTES } from './admin/admin.routing';

const APP_ROUTES: Routes = [
  { path: '', component: LandingComponent, pathMatch: 'full'},
  { path: 'session', component: AuthComponent },
  { path: 'dashboard', component: DashboardComponent },

  // { path: 'projects', children: PROJECT_ROUTES },
  // { path: 'admin', children: ADMIN_ROUTES },
];

export const Routing = RouterModule.forRoot(APP_ROUTES);
