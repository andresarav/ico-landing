import { Component, OnInit } from '@angular/core';
import Tilt from 'tilt.js';
import * as $ from 'jquery';
import Parallax from 'parallax-js';



@Component({
  selector: 'app-presupuesto',
  templateUrl: './presupuesto.component.html',
  styleUrls: ['./presupuesto.component.css']
})
export class PresupuestoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
        var scene = document.getElementById('scene');
        var parallaxInstance = new Parallax(scene);



//       var ctx = document.getElementById("myChart");
//
//       var myChart = new Chart(ctx, {
//     type: 'doughnut',
//     data: {
//       labels: ["cantidad1", "cantidad1", "cantidad1", "cantidad1", "cantidad1", "cantidad1"],
//         datasets: [{
//             data: [16, 4, 7, 7, 10, 56],
//             backgroundColor: [
//                 'rgb(96, 125, 139)',
//                 'rgb(76, 175, 80)',
//                 'rgb(0, 188, 212)',
//                 'rgb(3, 169, 244)',
//                 '#244f73',
//                 '#40f29b',
//             ],
//             borderColor: [
//               'black',
//               'black',
//               'black',
//               'black',
//               'black',
//               'black',
//             ],
//             // borderWidth: 3,
//         }]
//     },
//     options: {
//       legend:{
//         display: false
//       },
//       scales: {
//             xAxes: [{
//                 ticks: {
//                   callback: function(value, index, values) {
//                       return '';
//                   }
//                 }
//               }],
//         }
//     }
// });




Tilt;
const tilts = $('.percent').tilt({
  maxTilt: 40,
  glare: false,
  maxGlare: 0.3,
  scale: 1.2,
  transition:true,
  speed: 2000,
  easing: "cubic-bezier(.03,.98,.52,.99)",
  perspective:1000,
  reset:true
});

  }

}
