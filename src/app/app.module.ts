import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';




import { AppComponent } from './app.component';
import { EncabezadoComponent } from './encabezado/encabezado.component';
import { PresupuestoComponent } from './presupuesto/presupuesto.component';
import { HeaderComponent } from './header2/header.component';
import { RoadmapComponent } from './roadmap/roadmap.component';
import { FichaComponent } from './fichatecnica/ficha.component';
import { MembersComponent } from './members/members.component';
import { AuthComponent } from './auth/auth.component';
import { LandingComponent } from './landing/landing.component';
import { DashboardComponent } from './dashboard/dashboard.component';


import {SlideshowModule} from 'ng-simple-slideshow';

// Angular material
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {materialModule} from './material.module'
import 'hammerjs';


import { Routing } from './app.routing';


import { HttpModule } from '@angular/http';


import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { MomentModule } from 'angular2-moment';




@NgModule({
  declarations: [
    AppComponent,
    EncabezadoComponent,
    PresupuestoComponent,
    HeaderComponent,
    RoadmapComponent,
    FichaComponent,
    MembersComponent,
    AuthComponent,
    LandingComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    materialModule,
    BrowserAnimationsModule,
    HttpModule,
    SlideshowModule,
    Routing,
    FormsModule,
    ReactiveFormsModule,
    MomentModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

  }
