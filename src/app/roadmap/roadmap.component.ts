import { Component, OnInit, ViewChild } from '@angular/core';
import Tilt from 'tilt.js';
import * as $ from 'jquery';
declare let ScrollMagic: any;


@Component({
  selector: 'app-roadmap',
  templateUrl: './roadmap.component.html',
  styleUrls: ['./roadmap.component.css']
})


export class RoadmapComponent implements OnInit {

  @ViewChild('slideshow') slideshow;
  // @ViewChild() slideshow;

  constructor() {
    console.log('eta he la pantaita!!!!!!', screen.width);

    if(screen.width<600){
      this.movil = true;
    }
  }


  putisima(){
    this.slideshow.goToSlide(1)
  }

 movil:boolean = false;
 panelOpenState = false;
 yearRoadmap:any="2015";

   imageUrlArray = [
       '../../assets/roadmap/hito1/1.png',
       '../../assets/roadmap/hito1/2.png',
       '../../assets/roadmap/hito1/3.png',
       '../../assets/roadmap/hito1/4.png',
    ];


    hito1:boolean = false;

    imagesHito1 = [
        '../../assets/roadmap/hito1/1.png',
        '../../assets/roadmap/hito1/2.png',
        '../../assets/roadmap/hito1/3.png',
        '../../assets/roadmap/hito1/4.png',
     ];



     imgHito1Active:any = this.imagesHito1[3];


     hito2:boolean = false;

     imagesHito2 = [
       './assets/roadmap/hito2/1.jpg',
       './assets/roadmap/hito2/2.jpg',
       './assets/roadmap/hito2/3.jpg',
       './assets/roadmap/hito2/4.jpg'
      ];


      imgHito2Active:any = this.imagesHito2[1];

      hito3:boolean = false;

           imagesHito3 = [
             './assets/roadmap/hito3/1.png',
              './assets/microsoft.jpg',
            ];

       imgHito3Active:any = this.imagesHito3[1];



       hito4:boolean = false;

       imagesHito4 = [
         './assets/roadmap/hito4/1.jpg',
         './assets/roadmap/hito4/2.png',
         './assets/roadmap/hito4/3.png',

        ];

        imgHito4Active:any = this.imagesHito4[0];




        hito5:boolean = false;

        imagesHito5 = [
            './assets/nxtp.jpg',
            './assets/microsoft.jpg',
            './assets/Tropay.png',
            './assets/recorte.jpg',
            './assets/fondo.jpg',
            './assets/atomo.gif',
         ];

         imgHito5Active:any = './assets/microsoft.jpg';




         hito6:boolean = false;

         imagesHito6 = [
             './assets/nxtp.jpg',
             './assets/microsoft.jpg',
             './assets/Tropay.png',
             './assets/recorte.jpg',
             './assets/fondo.jpg',
             './assets/atomo.gif',
          ];

          imgHito6Active:any = './assets/microsoft.jpg';



          hito7:boolean = false;

          imagesHito7 = [
              './assets/nxtp.jpg',
              './assets/microsoft.jpg',
              './assets/Tropay.png',
              './assets/recorte.jpg',
              './assets/fondo.jpg',
              './assets/atomo.gif',
           ];

           imgHito7Active:any = './assets/microsoft.jpg';



           hito8:boolean = false;

           imagesHito8 = [
               './assets/nxtp.jpg',
               './assets/microsoft.jpg',
               './assets/Tropay.png',
               './assets/recorte.jpg',
               './assets/fondo.jpg',
               './assets/atomo.gif',
            ];

            imgHito8Active:any = './assets/microsoft.jpg';




            hito9:boolean = false;

            imagesHito9 = [
                './assets/nxtp.jpg',
                './assets/microsoft.jpg',
                './assets/Tropay.png',
                './assets/recorte.jpg',
                './assets/fondo.jpg',
                './assets/atomo.gif',
             ];

             imgHito9Active:any = './assets/microsoft.jpg';



             hito10:boolean = false;

             imagesHito10 = [
                 './assets/nxtp.jpg',
                 './assets/microsoft.jpg',
                 './assets/Tropay.png',
                 './assets/recorte.jpg',
                 './assets/fondo.jpg',
                 './assets/atomo.gif',
              ];

              imgHito10Active:any = './assets/microsoft.jpg';



              hito11:boolean = false;

              imagesHito11 = [
                  './assets/nxtp.jpg',
                  './assets/microsoft.jpg',
                  './assets/Tropay.png',
                  './assets/recorte.jpg',
                  './assets/fondo.jpg',
                  './assets/atomo.gif',
               ];

               imgHito11Active:any = './assets/microsoft.jpg';




               hito12:boolean = false;

               imagesHito12 = [
                   './assets/nxtp.jpg',
                   './assets/microsoft.jpg',
                   './assets/Tropay.png',
                   './assets/recorte.jpg',
                   './assets/fondo.jpg',
                   './assets/atomo.gif',
                ];

                imgHito12Active:any = './assets/microsoft.jpg';


                hito13:boolean = false;

                imagesHito13 = [
                    './assets/nxtp.jpg',
                    './assets/microsoft.jpg',
                    './assets/Tropay.png',
                    './assets/recorte.jpg',
                    './assets/fondo.jpg',
                    './assets/atomo.gif',
                 ];

                 imgHito13Active:any = './assets/microsoft.jpg';




  controller;
  ngOnInit() {



    this.controller = new ScrollMagic.Controller();

    // DISPARADOR HITO1  ----------------------------------------------------------------------


    var scene = new ScrollMagic.Scene({
      triggerHook:0.4,
      triggerElement:"#hito1"
   })
            // .addIndicators({
            //   name: 'PrimerHito'
            // })
            .addTo(this.controller);

            scene.on('start', (event) => {

              $('.YearAnim').toggleClass("gononea");
              $('#hitoC1').toggleClass("aparece");
              this.yearRoadmap = "2015";

            })


  // DISPARADOR HITO2  ----------------------------------------------------------------------


  var scene2 = new ScrollMagic.Scene({
    triggerHook:0.4,
    triggerElement:"#hito2"
 })
          // .addIndicators()
          .addTo(this.controller);

          scene2.on('start', (event) => {
            $('#hitoC1').toggleClass("aparece");
            $('#hitoC2').toggleClass("aparece");
            this.yearRoadmap = "2016";

            // $('#contenidoH1').toggleClass("desaparecelo");
            // $('#contenidoH2').toggleClass("aparecelo");

            // contenidoH2
            // contenidoH1
          })

//
// // DISPARADOR HITO3  ----------------------------------------------------------------------
//

        var scene3 = new ScrollMagic.Scene({
          triggerHook:0.4,
          triggerElement:"#hito3"
       })
                // .addIndicators()
                .addTo(this.controller);

                scene3.on('start', (event) => {
                  $('#hitoC2').toggleClass("aparece");
                  $('#hitoC3').toggleClass("aparece");

                })
//
//
//
// // DISPARADOR HITO4  ----------------------------------------------------------------------
//

          var scene4 = new ScrollMagic.Scene({
            triggerHook:0.4,
            triggerElement:"#hito4"
         })
                  // .addIndicators()
                  .addTo(this.controller);

                  scene4.on('start', (event) => {
                    this.yearRoadmap = "2017";
                    $('#hitoC3').toggleClass("aparece");
                    $('#hitoC4').toggleClass("aparece");


                  })


// DISPARADOR HITO5  ----------------------------------------------------------------------


                var scene5 = new ScrollMagic.Scene({
                  triggerHook:0.4,
                  triggerElement:"#hito5"
               })
                        // .addIndicators()
                        .addTo(this.controller);

                        scene5.on('start', (event) => {
                          this.yearRoadmap = "2018";
                          $('#hitoC4').toggleClass("aparece");
                          $('#hitoC5').toggleClass("aparece");
                        })


  // DISPARADOR HITO6  ----------------------------------------------------------------------

        var scene5 = new ScrollMagic.Scene({
          triggerHook:0.4,
          triggerElement:"#hito6"
       })
                // .addIndicators()
                .addTo(this.controller);

                scene5.on('start', (event) => {
                  $('#hitoC5').toggleClass("aparece");
                  $('#hitoC6').toggleClass("aparece");
                })


    // DISPARADOR HITO7  ----------------------------------------------------------------------

    var scene5 = new ScrollMagic.Scene({
      triggerHook:0.4,
      triggerElement:"#hito7"
   })
            // .addIndicators()
            .addTo(this.controller);

            scene5.on('start', (event) => {
              $('#hitoC6').toggleClass("aparece");
              $('#hitoC7').toggleClass("aparece");
            })



      // DISPARADOR HITO8  ----------------------------------------------------------------------

        var scene5 = new ScrollMagic.Scene({
          triggerHook:0.4,
          triggerElement:"#hito8"
       })
                // .addIndicators()
                .addTo(this.controller);

                scene5.on('start', (event) => {
                  $('#hitoC7').toggleClass("aparece");
                  $('#hitoC8').toggleClass("aparece");
                })



      // DISPARADOR HITO9  ----------------------------------------------------------------------

        var scene5 = new ScrollMagic.Scene({
          triggerHook:0.4,
          triggerElement:"#hito9"
       })
                // .addIndicators()
                .addTo(this.controller);

                scene5.on('start', (event) => {
                  this.yearRoadmap = "2019";
                  $('#hitoC8').toggleClass("aparece");
                  $('#hitoC9').toggleClass("aparece");
                })



      // DISPARADOR HITO10  ----------------------------------------------------------------------

          var scene5 = new ScrollMagic.Scene({
            triggerHook:0.4,
            triggerElement:"#hito10"
         })
                  // .addIndicators()
                  .addTo(this.controller);

                  scene5.on('start', (event) => {
                    $('#hitoC9').toggleClass("aparece");
                    $('#hitoC10').toggleClass("aparece");
                  })



    // DISPARADOR HITO11  ----------------------------------------------------------------------

        var scene5 = new ScrollMagic.Scene({
          triggerHook:0.4,
          triggerElement:"#hito11"
       })
                // .addIndicators()
                .addTo(this.controller);

                scene5.on('start', (event) => {
                  $('#hitoC10').toggleClass("aparece");
                  $('#hitoC11').toggleClass("aparece");
                })




                // DISPARADOR HITO12  ----------------------------------------------------------------------

                    var scene5 = new ScrollMagic.Scene({
                      triggerHook:0.4,
                      triggerElement:"#hito12"
                   })
                            // .addIndicators()
                            .addTo(this.controller);

                            scene5.on('start', (event) => {
                              $('#hitoC11').toggleClass("aparece");
                              $('#hitoC12').toggleClass("aparece");
                            })


            // DISPARADOR HITO12  ----------------------------------------------------------------------

                var scene5 = new ScrollMagic.Scene({
                  triggerHook:0.4,
                  triggerElement:"#hito13"
               })
                        // .addIndicators()
                        .addTo(this.controller);

                        scene5.on('start', (event) => {
                          $('#hitoC12').toggleClass("aparece");
                          $('#hitoC13').toggleClass("aparece");
                        })




// Tilt;
// const tilts = $('.percent').tilt({
//   maxTilt: 40,
//   glare: false,
//   maxGlare: 0.3,
//   scale: 1.2,
//   transition:true,
//   speed: 2000,
//   easing: "cubic-bezier(.03,.98,.52,.99)",
//   perspective:1000,
//   reset:true
// });

  }

}
